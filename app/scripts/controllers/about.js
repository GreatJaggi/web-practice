'use strict';

/**
 * @ngdoc function
 * @name webPracticeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the webPracticeApp
 */
angular.module('webPracticeApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
