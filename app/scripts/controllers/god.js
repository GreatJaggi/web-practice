var app = angular.module('webPracticeApp', []);

        app.controller('PlayerController', function($interval) {
            var Player = this;
            Player.pictureSet = [
                                "images/rock.jpg",
                                "images/paper.png",
                                "images/scissors.png"
                                
                                ];

            Player.picture = ""
            Player.getPicture = function(value)    {
                    Player.picture = Player.pictureSet[value];
            };//function

            Player.count = 0;
            Player.score = 0;
            Player.opScore = 0;

            Player.counting = true;
            Player.timerDuration = 20;

            Player.varx = 0;

            $interval(function() {
                if(Player.counting) {
                    if(Player.count < 2)
                        Player.count++;
                    else Player.count = 0;

                    Player.timerDuration--;
                    //console.log(Player.timerDuration);
                    if(Player.timerDuration == 0)   {
                        Player.count = (Math.floor(Math.random() * 3))
                        Player.timerDuration = 20;
                        Player.counting = false;
                    }
                }//if

                Player.getPicture(Player.count);
            },70)

            Player.inCount = 4;

            Player.gameInfo   = function() {
                if(Player.counting == true)
                    return "Ready!";
                if(Player.counting == false)
                    return "Answer in " + (Player.inCount + 1);
            }

            $interval(function()    {
            if(Player.counting == false)    {
                if(Player.inCount == 0)    {
                    Player.inCount = 5;
                    Player.counting = true;
                    Player.command();
                }//if
                Player.inCount--;
            }//if
                
            },1000)
            

            Player.actionSet = [
                                "Rock", 
                                "Paper", 
                                "Scissor",
                                "Attack"
                                ];
            Player.action = "";

            Player.whoWins = function(i)    {
                switch(i)   {
                    case 0: 
                        if(Player.count == 2)
                            Player.score += 1;
                        else if(Player.count == 1)
                            Player.opScore += 1;
                        break;

                    case 1: 
                        if(Player.count == 0)
                            Player.score += 1;
                        else if(Player.count == 2)
                            Player.opScore += 1;
                        break;

                    case 2: 
                        if(Player.count == 1)
                            Player.score += 1;
                        else if(Player.count == 0)
                            Player.opScore += 1;
                        break;
                }//switch
                return true;
            }//whoWins

            Player.command = function()   {
                for(i = 0; i < Player.actionSet.length; i++)    {
                    if(Player.counting == false)    {
                        if(Player.actionSet[i].toUpperCase() == Player.action.toUpperCase())    {
                            //console.log("Player " + Player.actionSet[i] + "ed");
                            //Player.whoWins(i);
                            //console.log(Player.Counting);
                            Player.counting = Player.whoWins(i);
                            //console.log(Player.Counting);
                            break;
                        }//if
                        else if(i == Player.actionSet.length - 1)   {
                            Player.counting = Player.whoWins(i);
                            console.log("Unknown Command");
                        }
                    }
                }//for
                Player.action = "";
                Player.inCount = 4;
            };//function command
        });
