'use strict';

/**
 * @ngdoc function
 * @name webPracticeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webPracticeApp
 */
angular.module('webPracticeApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
