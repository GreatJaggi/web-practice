'use strict';

/**
 * @ngdoc overview
 * @name webPracticeApp
 * @description
 * # webPracticeApp
 *
 * Main module of the application.
 */
angular
  .module('webPracticeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/ts', 	{
      	templateUrl: 'views/table_samp.html',
      	controller: 'TableCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
